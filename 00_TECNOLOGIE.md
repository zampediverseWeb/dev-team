# TECNOLOGIE

### HTML
- Usa l'html con attenzione; assicurati di chiudere il markup, utilizza gli attributi che sono conformi agli standard; fatti un giro su w3schools se hai dubbi
```html
    <div id="myidentifier">
        <!-- elementi che si chiudono da soli -->
        <hr />
        <br />
        <input />
        <image />
        <link />
    </div>
```

- le classi le uso N volte, l'ID è univoco
- indento bene il markup

### I fogli di stile
Scrivi l'SCSS ben indentato; se uno stile viene ripetuto in più parti usa i mixins o l'extend della classe
```scss
    /** 
    * esempio di extend
    */
    .myclass {
        color: #fff;
        background: red;
        border: 2px solid red;
    }
    .unelementosimile {
        @extend .myclass;
        border-color: green;
    }

    /** 
    * esempio di mixin
    */
    @mixin myclass {
        color: #fff;
        background: red;
        border: 2px solid red;
    }
    .mioelemento {
        @include myclass;
    }
```

- Non usare le percentuali, tranne in specifici casi, preferisci i px o i vw e vh;
- non usare mai 0px/0vw/0%: ZERO è un valore assoluto, indipendentemente dall'unità di misura
- gli important sono il male, evitali come la peste. Una buona selezione dei selettori funziona sempre, e ricordati che a parità di selezione, comanda l'ultima.

### Vanilla.js vs jQuery
- abituati ad abbandonare jQuery perchè lo abbandoneremo quanto prima.
```javascript
    /**
    * INSERISCO DEL TESTO 
    */
    jQuery('#myidentifier').html('Hello world!'); // jQuery

    document.getElementById('myidentifier').innerHtml = 'Hello world!'; // Vanilla

    /**
    * FUNZIONE AL CLICK 
    */
    var mybtn = jQuery('#btn');
        btn.on('click',function(e){
            e.preventDefault();
            /* funzione del click */
        }); // jQuery

    var mybtn = document.getElementById('btn');
        mybtn.addEventListener('click',function(e){
            e.preventDefault();
        });
```
Approfondisci su: http://youmightnotneedjquery.com/

- Non buttarti a scrivere mai funzioni a caso: organizzale, fai in modo che possano essere riutilizzate, fai che siano quanto più agnostiche possibile così da poterle estendere
- ricordati che il `this` è il tuo scope; usa le clousure, rendi globale le variabili che hanno necessità di essere esternalizzate

## Le tecnologie da usare

Utilizziamo tecnologie che già conosciamo, che abbiamo testato, che sono state studiate; non avventurarti in qualcosa di nuovo da solo che gli altri non conoscono

**Backend**
* PHP 7 (no: Php 5, Ruby, Python, Java)
* LARAVEL e WORDPRESS

**Frontend**
* jQuery (ok Vanilla, no: Mootools, Ext JS, YUI, Dojo, Prototype)
* Moment.js (no: new Date di javascript)
* Gulp (no: Webpack, Grunt)
* Handlebars (no: Vue, Angular, React, Ember)
* Scss (no: SASS - LESS)
* HTML (no: JADE)
* Bootstrap 3/4 (no: Foundation, Material Design)

## BREAKPOINT

- utilizza il concetto di Mobile first: tutti i breakpoint partono dal concetto di max-width
```css
    @media (max-width: 1024px){
        /**
        * code here
        */
    }
```
- il desktop è a partire dai breakpoint superiori al 1024
- il mobile parte da 768px
- lo smartphone parte da 414px

### GIT

Comandi principali:
```bash
- git add . #aggiungo tutti i file modificati
- git commit -m 'descrivo ciò che ho fatto' # versiono lo stato attuale del codice
- git pull origin <branch> # scarico l'ultimo aggiornamento
- git push origin <branch> # carico il mio aggiornamento
```

Alcuni consigli:

1. committa e pusha: prima di andare via ricordati sempre di aver sincronizzato le repo su cui hai lavorato
2. utilizza bitbucket per poter accedere alle versioni precedenti del file
3. Git è un software: bibucket, github, gitlab, sono applicativi che forniscono il software git e lo integrano con altri loro servizi

[Approfondimenti](/04_GIT.md)


### SSH & FTP

Ti ricordo che su i server non esiste un cestino, una volta che il file è cancellato, è per sempre.

Connessione ad un ftp:
- usa la porta 21
- inserisci host
- inserisci username
- inserisci password

Connessione Sftp:
- è uguale ad ftp, ma la porta è la 22

Connessione SSH
- apri il terminale e digita:
```shell
    > ssh $username@$host
    > $password
```

### GULP

Gulp è un task runner; non è magia, fa esattamente ciò che gli dici, ovvero esegue dei task che tu hai programmato.

*I comandi che utilizziamo in zampe*
```php
    /**
    * Lavorare in locale, avviare un server e compilazione live dei file
    */
    gulp watch
```

```php
    /**
    * Buildare "O" creare il pacchetto
    * $env = 'local', 'stage', 'dev', 'prod';
    */

    gulp --env $env
```

```php
    /**
    * Deploy
    * $env = 'local', 'stage', 'dev', 'prod';
    */

    gulp deploy --dest $env
```