## Progetti IKEA

DATA INIZIO: ______________________  

NOME PROGETTO: ______________________  

DEV: ______________________________

TESTER: ______________________________

### Bootstrap nuovo progetto

* [ ] creazione repository
* [ ] verifico se la pagina è già stata creata su TeamSite
* [ ] creo la cartella su Azure, ed un file html.js con un qualsiasi console.log()
* [ ] fornisco il lo script html.js a denise che lo inserisce su teamsite
* [ ] creo la device detection (trovo tutte le informazioni su best practices sul server Z)
* [ ] attendo che la pagina salga su teamsite, e verifico che siaperfettamente linkata dal browser
* [ ] inserisco su redbooth le URL desktop e mobile, di Dev e Prod


### FUNZIONALITA'

* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________
* [ ] _______________________________________________________________________________________

### Checklist per il rilascio o aggiornamento di un progetto IKEA

* [ ] SVILUPPO
* [ ] eseguo una ad una le funzionalità; barro quelle OK, fixo quelle che non vanno
* [ ] porto su stage per poter testare
* [ ] verifico date di timing sul codice
* [ ] verifico che sul backoffice ci sia l'import corretto
* [ ] verifico che sul backoffice ci siano le date corrette
* [ ] verifico che il bottone compra online funzioni su desktop e mobile
* [ ] verifico il comportamento responsive
* [ ] verifico che i testi siano quelli esatti
* [ ] verifico che la device detection faccia correttamente il redirect desktop e mobile
* [ ] DEBUG
* [ ] porto in produzione
* [ ] verifico che si veda correttamente anche in produzione
* [ ] riverifico tutte le funzionalità previste al punto 1
* [ ] ri-verifico che l'add to cart aggiunga al carrello
* [ ] informo su redbooth e chiedo a Luca di risolvere la task

### Checklist di progetto

* [ ] Flag su task di redbooth
* [ ] Taggo un grafico per fare un check
* [ ] Check accessi siano indicati sul Wordkspace
* [ ] Check URL di test e prod siano indicati sul Workspace