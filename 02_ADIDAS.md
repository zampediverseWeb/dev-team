### Progetti ADIDAS

DATA INIZIO: ______________________  

NOME PROGETTO: ______________________  

DEV: ______________________________

* [ ] studio la grafica insieme al Designer, individuo criticità, chiedo più informazioni e propongo soluzioni
* [ ] creo la repo
* [ ] creo la cartella sul server di stage e prod
* [ ] scrivo mail al referente del SIS con script. LO SCRIPT SEMPRE IN HTTPS
* [ ] inizializzo il progetto come Static site (gulpfile, cartelle app, enviroments)
* [ ] configuro il gulpfile con le path e percorsi al server
* [ ] sviluppo
* [ ] riciclo le funzionalità simili già presenti in precedenti rilasci
* [ ] inserisco i tracciamenti su i bottoni ed eventi
* [ ] riverifico insieme al Designer
* [ ] scrivo mail a referente del SIS per avere i link di atterraggio
* [ ] aggiorno i link, su desktop e mobile
* [ ] dopo OK interno, rilascio in stage
* [ ] condivido la preview con Erica
* [ ] porto in Produzione e lo comunico ad Erica