### Altri progetti (REDBULL, UAE, GEMS, AIE)

DATA INIZIO: ______________________  

NOME PROGETTO: ______________________  

DEV: ______________________________

* [ ] studio la grafica insieme al Designer, individuo criticità, chiedo più informazioni e propongo soluzioni
* [ ] creo la repo
* [ ] chiedo i riferimenti del server di stage/prod
* [ ] verifico le connessioni FTP, DB ed eventuali CMS esistenti
* [ ] analizzo il progetto con il dev team e si sceglie la tecnologia migliore
* [ ] sviluppo
* [ ] riciclo le funzionalità sviluppate in altri progetti
* [ ] verifico insieme al designer tutte le funzionalità
* [ ] verifico insieme al team
* [ ] dopo OK interno porto su Stage
* [ ] verifico che le funzionalità girino anche su stage
* [ ] mando preview al cliente/giro la preview a Luca che la gira al cliente
* [ ] pubblico su prod