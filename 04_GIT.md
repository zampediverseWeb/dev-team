## Gestione commit
Operazioni per aggiungere la tua porzione di codice alla repo
```bash
$ git add . #aggiungo tutti i file modificati
$ git commit -m 'descrivo ciò che ho fatto' # versiono lo stato attuale del codice
$ git pull origin <branch> # scarico l'ultimo aggiornamento
$ git push origin <branch> # carico il mio aggiornamento
```

## Merge branch to master
Unisco il codice di un altro branch, col ramo master
```bash
$ git checkout master
$ git pull origin master
$ git merge $nomeBranch
$ git push origin master
```

## New Branch
```bash
$ git checkout -b <newbranch>
$ git push origin <newbranch>
```

## Reset all'ultimo commit
Se ho delle modifiche da annullare
```bash
$ git reset HEAD --hard
```

## Remove directory from repo
```bash
$ git rm -r --cached <dirname>
```

## Remove file from repo
```bash
$ git rm --cached <dirname>
```