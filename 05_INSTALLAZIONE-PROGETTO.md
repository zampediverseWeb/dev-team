## COME INSTALLARE UN PROGETTO

- entrare dentro il relativo progetto in bitbucket
- cliccare su clone, e copiare il comando per clonare la repo
- aprire il terminale (CMD + SPACE) e puntare la path di lavoro con il comando CD (chage directory)
- entrare nella cartella di progetto dopo averlo clonato
- se ho il file "package.json" -> lanciare il comando "npm i"
- se ho il file "bower.json" -> lanciare il comando "bower i"
- lanciare il comando "gulp watch"
- dopo che si apre il browser alla URI localhost:3000, aggiungere il puntamento a /preview-desktop.html