## GENERARE UN QRCODE

Per generare un QR code, basta richiamare la seguente URL e aggiungere i parametri:
https://chart.googleapis.com/chart

PARAMETRI:  
```bash
chs={{$width}}x{{$height}}
cht=qr
chl={{$url}}
```
Qui un esempio del risultato finale:

```html
<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=https://www.calcioshop.it/%2F&choe=UTF-8" alt="qrcode" />
```