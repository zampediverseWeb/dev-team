# DEV_TEAM: GUIDELINES

## Punti generali di un buon Dev in Zampediverse

### Accetta i tuoi punti deboli e chiedi aiuto

Puoi avere tante lacune nelle tue conoscenze, specie all'inizio, e non c'è motivo di nasconderlo, accettalo e chiedi aiuto quando ne hai bisogno, altrimenti perderai tempo inutile a sbatterci la testa, e farai solo la figura dell'inconcludente.

### Prendi nota

Come al punto uno, è un diritto e dovere chiedere aiuto, ma dovresti evitare di fare la stessa domanda più volte. Quindi, ogni volta che ricevi aiuto, ricorda di annotare la soluzione in modo tale che la prossima volta che ti trovi nella stessa situazione avrai la possibilità di cavartela da solo, e magari potrai anche aiutare qualcun'altro che avrà il tuo stesso problema.

Non ci sono domande stupide, ma se hai già fatto la stessa domanda, allora è stupida.

### Sembra più complicato di quello che è

Questo è un principio che si riscontra ogni volta che si ha a che fare con qualcosa di nuovo: meno si conosce un argomento, più sembra complesso.

Leggi un articolo su internet, guardati un video, prenditi tutto il tempo necessario per approfondire, vedrai che non è così spaventoso come sembra.

### Se è troppo complesso: visualizzalo

Ci sono progetti semplici e progetti complessi. Mentre in quelli semplici, è veloce avere uno schema in testa su quello che deve essere fatto, su quelli più complessi è bene provare a disegnare uno schema su un foglio di carta.

Immagina il nostro cervello come una memoria RAM, con tante informazioni diventa complesso immagizzinare tante variabili, percorsi e meccanismi; il foglio di carta sarà il tuo hard-disk,dove gli schemi rimangono e ti aiuteranno a tenere traccia di tutto il flusso.

### Alla scoperta dei bug

La cosa più divertente del nostro lavoro è creare nuove funzionalità, ma fare solo questo non accrescerà le tue competenze.

Scoprire e fixare nuovi bug, invece, ti permetterà intanto di migliore le funzionalità esistenti e renderle perfettamente funzionanti, ma sopratutto ti porterà a trovare soluzioni, trick e hack; capirai certi meccanismi, approcci più sicuri e veloci, che ti renderanno la vita più semplice la prossima volta che svilupperai la stessa funzionalità.

## GUIDA ALLO SVILUPPO

- [TECNOLOGIE](/00_TECNOLOGIE.md)
- [PROGETTI IKEA](/01_IKEA.md)
- [PROGETTI ADIDAS](/02_ADIDAS.md)
- [REDBULL / UAE / GEMS / AIE](/03_ALTRI.md)

## GUIDE TECNICHE
- [GIT e REPO](/04_GIT.md)
- [INSTALLAZIONE PROGETTO](/05_INSTALLAZIONE-PROGETTO.md)
- [TIPS & TRICKS veloci](/10_TIPS_AND_TRICKS.md)